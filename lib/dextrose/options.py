import sys
import argparse


class Options:
    """Command-line options

    Handles building of command-line parser using argparse, including
    uage info. Parses arguments and stores the results.
    """

    def __init__(self, app):
        """Initializer

        Ready the command-line parser and parse the arguments.
        """
        self.app = app
        self.build_parser()
        self.args = self.parser.parse_args()
        self.validate_args()

    def build_parser(self):
        """Build the command line parser

        Construct the command-line parser and usage info.
        """
        parser = argparse.ArgumentParser(
            prog="dextrose",
            description="CLI MIDI Librarian for the DX7II-FD/D",
        )

        global_group = parser.add_argument_group(
            "GLOBAL OPTIONS", "Options that are useful everywhere"
        )
        global_group.add_argument(
            "-f",
            "--file",
            help="name of file to pull to, alter, or push from",
            nargs="?",
            const="dump.syx",
            dest="filename",
        )
        global_group.add_argument(
            "-v",
            "--verbose",
            help="",
            nargs="?",
            const=True,
            dest="list_midi_ports",
        )

        verb_group = parser.add_argument_group(
            "VERBS", "Actions you can take"
        )
        verb_group.add_argument(
            "verb",
            nargs=1,
            type=str,
            choices=[
                "pull",
                "transform",
                "push",
                "list"
            ],
            help="Actions you can take on data types",
        )

        noun_group = parser.add_argument_group(
            "NOUNS", "Data types you can perform actions on"
        )
        noun_group.add_argument(
            "noun",
            nargs=1,
            type=str,
            choices=[
                "voice_edit_buffer",
                "supplement_edit_buffer",
                "packed_32_supplement",
                "packed_32_voice",
                "ports",
            ],
            help="Data types",
        )

        # process_group = parser.add_argument_group(
        #     "PROCESS CONTROL", "Options that control the application process"
        # )
        # process_group.add_argument(
        #     "-C",
        #     "--show-config",
        #     help="show merged configuration options and exit",
        #     dest="show_config",
        #     action="store_true",
        # )
        # process_group.add_argument(
        #     "-c",
        #     "--config",
        #     help="specify a configuration file",
        #     dest="conf_file",
        # )
        # process_group.add_argument(
        #     "-g",
        #     "--loglevel",
        #     help="log level",
        #     choices=["error", "warning", "info", "debug"],
        #     dest="log_level",
        # )
        # process_group.add_argument(
        #     "-o",
        #     "--errout",
        #     help="redirect stderr to a file",
        #     dest="stderr_file",
        # )
        # process_group.add_argument(
        #     "-d",
        #     "--daemonize",
        #     help="[legacy] run process in background",
        #     dest="daemonize",
        #     action="store_true",
        # )
        # process_group.add_argument(
        #     "-k",
        #     "--kill",
        #     help="[legacy] terminate background process",
        #     dest="kill_process",
        #     action="store_true",
        # )
        # process_group.add_argument(
        #     "-r",
        #     "--restart",
        #     help="[legacy] restart background process",
        #     dest="restart_process",
        #     action="store_true",
        # )
        #
        # server_group = parser.add_argument_group(
        #     "SERVER CONFIGURATION", "Options that control web service"
        # )
        # server_group.add_argument(
        #     "-b", "--bind", help="TCP bind address", dest="bind_address"
        # )
        # server_group.add_argument(
        #     "-p", "--port", help="TCP listen port", dest="listen_port"
        # )
        # server_group.add_argument(
        #     "-u",
        #     "--user",
        #     help="run server as this user",
        #     dest="process_user",
        # )
        # server_group.add_argument(
        #     "-s",
        #     "--server-header",
        #     help="server name/version for HTTP headers",
        #     dest="server_header",
        # )
        # server_group.add_argument(
        #     "-x",
        #     "--render-extensions",
        #     help="comma-separated list of filename extensions "
        #     "that should be processed by Mako",
        #     dest="render_extensions",
        # )

        self.parser = parser

    def validate_args(self):
        #  Ensure only one legacy daemon function was used if any were used
        actions = {
            "daemonize": "-d/--daemonize",
            "kill_process": "-k/--kill",
            "restart_process": "-r/--restart",
        }
        requested = [
            v for k, v in actions.items() if getattr(self.args, k, False)
        ]
        if len(requested) > 1:
            print(
                "Multiple mutually exclusive actions were specified:\n   ",
                ", ".join(requested),
                "\nUse -h or --help for detailed help on these options.",
            )
            sys.exit(2)

    def get_args(self):
        """Get parsed command-line arguments

        Returns:
            command line arguments as argparse namespace
        """
        return self.args
