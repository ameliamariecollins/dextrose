"Dextrose main application module."

# import sys
import saturnv
import mido

from .options import Options
from .midi.sysex import BulkDumpRequest


class Application:
    """Dextrose application object"""

    default_conf = {
        "log_level": "warning",
        "verbose": False,
    }

    def __init__(self):
        self.log = saturnv.Logger()
        self.log.set_level(self.default_conf["log_level"])  # from defaults
        self.log.stderr_on()

        self.options = Options(self)
        self.args = self.options.get_args()
        self.conf = saturnv.AppConf(
            defaults=Application.default_conf,
            args=self.args,
        )
        self.noun = self.conf["noun"][0]
        self.log.set_level(self.conf["log_level"])  # from final config

    def run(self):
        getattr(self, f"action_{self.conf['verb'][0]}")()

    def action_list(self):
        print(
            "\n".join(
                [
                    "Input Ports:",
                    "\n".join(mido.get_input_names()),
                    "\nOutput Ports:",
                    "\n".join(mido.get_output_names()),
                ]
            )
        )

    def action_pull(self):
        output_port_name = "CH345:CH345 MIDI 1 16:0"
        input_port_name = "CH345:CH345 MIDI 1 16:0"

        with mido.open_output(output_port_name) as output_port, mido.open_input(
            input_port_name
        ) as input_port:
            dump_request = BulkDumpRequest(
                self.noun,
                0x00,  # HARDCODE: Device ID 1
            )

            response = dump_request.send(input_port, output_port)
            print(response)

    def action_transform(self):
        pass

    def action_push(self):
        pass

    def action_list_midi_ports(self):
        pass
