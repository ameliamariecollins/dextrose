# MIDI Message Status Bytes.

# Status bytes: Channel voice messages.
# (Least significant nibble is MIDI channel.)
STATUS_NOTE_OFF = 0x80
STATUS_NOTE_ON = 0x90
STATUS_AFTERTOUCH = 0xA0
STATUS_PROGRAM_CHANGE = 0xC0
STATUS_CHANNEL_PRESSURE = 0xD0
STATUS_PITCH = 0xE0

# Status bytes: Channel mode messages.
# (Least significant nibble is MIDI channel.)
STATUS_CONTROLLER = 0xB0

# Status bytes: System exclusive messages
STATUS_SYSEX_BEGIN = 0xF0

# Status bytes: System common messages
STATUS_MTC_QUARTER_FRAME = 0xF1
STATUS_SONG_POSITION_POINTER = 0xF2
STATUS_SONG_SELECT = 0xF3
STATUS_TUNE_REQUEST = 0xF6
STATUS_SYSEX_END = 0xF7

# Status bytes: System real-time messages
STATUS_MIDI_CLOCK = 0xF8
STATUS_MIDI_START = 0xFA
STATUS_MIDI_CONTINUE = 0xFB
STATUS_MIDI_END = 0xFC
STATUS_ACTIVE_SENSE = 0xFD
STATUS_RESET = 0xFE
