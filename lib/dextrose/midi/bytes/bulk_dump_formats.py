# MIDI Message Bulk Dump Format Bytes

bulk_dump_formats = {
    "voice_edit_buffer": {"format_num": 0x09, "transmit_time": 0.3},
    "supplement_edit_buffer": {"format_num": 0x05, "transmit_time": 0.3},
    "packed_32_supplement": {"format_num": 0x06, "transmit_time": 0.6},
    "packed_32_voice": {"format_num": 0x09, "transmit_time": 2.3},
}

universal_bulk_dump_class_name = [0x4C, 0x4D, 0x20, 0x20]

universal_bulk_dump_formats = {
    "univ_performance_edit_buffer": {
        "format_name": [0x38, 0x39, 0x37, 0x33, 0x50, 0x45],
        "transmit_time": 0.1,
    },
    "univ_packed_32_performance": {
        "format_name": [0x38, 0x39, 0x37, 0x33, 0x50, 0x4D],
        "transmit_time": 0.1,
    },
    "univ_system_setup": {
        "format_name": [0x38, 0x39, 0x37, 0x33, 0x53, 0x20],
        "transmit_time": 0.1,
    },
}

# Micro Tuning Edit Buffer: MCRYE_
# Micro Tuning with Memory #x: MCRYMx
# Micro Tuning Cartridge: MCRYC_
# Fractional Scaling Edit Buffer: FKSYE_
# Fractional Scaling in Cartridge with Memory # :FKSYC_
