"""Bulk Dump Request Object"""

# from copy import copy
import time
from mido import Message

from ..bytes import vendor
from ..bytes.bulk_dump_formats import bulk_dump_formats


class BulkDumpRequest:
    def __init__(
        self,
        format_name,
        device_id=0x00,  # 0x00-0x0f for device 1-16 ("substatus" in the docs)
        vendor_id=vendor.MANUFACTURER_ID_YAMAHA,  # 0x43 for Yamaha
    ):
        self.message = Message(
            "sysex",
            data=[
                vendor_id,
                device_id & 0x0F | 0x20,
                bulk_dump_formats[format_name]["format_num"],
            ],
        )
        self.format_name = format_name

    def send(self, input_port, output_port):
        """Fetch data from the synthesizer."""

        # Send request message
        print("[<] Requesting data from synthesizer")
        output_port.send(self.message)

        # Collect data
        print("[>] Receiving data from synthesizer")
        time.sleep(bulk_dump_formats[self.format_name]["transmit_time"])
        msg = input_port.receive(block=False)
        if msg is None:
            return None
        if msg.type == "sysex":
            mnum = len(msg)
            print(f"[=] {mnum} byte{'s' if mnum > 1 else ''} " "received")
            return msg
        else:
            print(f"[?] Non-sysex message received (type was {msg.type}")
            return msg
